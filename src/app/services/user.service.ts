import { Injectable } from '@angular/core';
import {Observable} from 'rxjs'
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  //===============Blog management

  addBlogs(data: any): Observable<any>{
    return this.http.post('http://localhost:10010/addBlogs', data)
  }

  listBlogs():Observable<any>{
    console.log("fnjcusjfc");
    return this.http.get('http://localhost:10010/listingBlogs')
  }

  editBlogs(id, data): Observable<any>{ 
    return this.http.put('http://localhost:10010/editBlogs?id=' + id,data);      
  }

  delete(id): Observable<any> {
    return this.http.delete('http://localhost:10010/deleteBlogs?id='+id)
  }

  onFileChange(event): Observable<any> {
    return this.http.delete('http://localhost:10010/imageBlogs')
  }

  //============User management

  listUsers():Observable<any>{
    console.log("listUsers");
    return this.http.get('http://localhost:10010/listingUsers')
  }

  addUsers(data: any): Observable<any>{
    return this.http.post('http://localhost:10010/addUsers', data)
  } 

  editUsers(id, data): Observable<any>{ 
    return this.http.put('http://localhost:10010/editUsers?id=' + id,data);      
  }

  deleteUsers(id): Observable<any> {
    return this.http.delete('http://localhost:10010/deleteUsers?id='+id)
  }

  //==============products management
  
  listProducts():Observable<any>{
    console.log("listProducts");
    return this.http.get('http://localhost:10010/listingProducts')
  }
  deleteProducts(id): Observable<any> {
    return this.http.delete('http://localhost:10010/deleteProducts?id='+id)
  }
  addProducts(data: any): Observable<any>{
    return this.http.post('http://localhost:10010/addProducts', data)
  }
  editProducts(id, data): Observable<any>{ 
    return this.http.put('http://localhost:10010/editProducts?id=' + id,data);      
  }

  //========add comments and rating
  comments(data): Observable<any>{ 
    console.log("data*******", data)
    return this.http.post('http://localhost:10010/addBlogsComments', data);      
  }

  commentsListing(id):Observable<any>{
    console.log("================service commentsListing", id);
    return this.http.get('http://localhost:10010/listingComments?id='+id)
  }
  getBlogById(id):Observable<any>{
    // console.log("================service commentsListing", id);
    return this.http.get('http://localhost:10010/getBlogById?id='+id)
  }}
