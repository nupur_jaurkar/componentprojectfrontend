import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
// import { Observable } from 'rxjs/Observable';

@Injectable({
  providedIn: 'root'
})
export class AuthServicesService {

  constructor(private http :HttpClient) { }
  
  getToken() {
    return localStorage.getItem("token")
  }
  isLoggednIn() {
    return this.getToken() !== null;
  }
  
}
