import { Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { UserService } from '../../../../../services/user.service';
import { ReactiveFormsModule } from "@angular/forms";
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-products',
  templateUrl: './add-products.component.html',
  styleUrls: ['./add-products.component.css']
})
export class AddProductsComponent implements OnInit {
  rForm: FormGroup;
  post:any;
  productName:string='';
  productDescription:string='';
  productCost:number;
  productCategory:string='';
  file:null;

  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(private fb: FormBuilder,
    private service:UserService, private router: Router,) { 
      this.rForm = fb.group({
        'productName' : [null, Validators.required],
        'productDescription' : [null, Validators.required],
        'productCost' : [null, Validators.required],
        'productCategory' : [null, Validators.required],
        file :null
        
        // 'file':[null,Validators.required]
      });
  }

  ngOnInit() {
  }

  onFileChange(event) {
    if(event.target.files.length > 0) {
      let file = event.target.files[0];
      this.rForm.get('file').setValue(file);
    }
  }
  
  private prepareSave(): any {
    let input = new FormData();
    input.append('productName', this.rForm.get('productName').value);
    input.append('productDescription', this.rForm.get('productDescription').value);
    input.append('productCost', this.rForm.get('productCost').value);
    input.append('productCategory', this.rForm.get('productCategory').value);    
    input.append('file', this.rForm.get('file').value);
    return input;
  }
  
  addProducts(post:any) {
    const formModel = this.prepareSave();
      this.service.addProducts(formModel).subscribe((res:any)=>{
        console.log("Your blog is saved!::",res)
        if(res.code=200){
          alert(res.message)
        }
      })   
  }
  

}
