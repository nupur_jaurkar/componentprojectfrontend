import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../../../../../services/user.service';

@Component({
  selector: 'app-blog-listing',
  templateUrl: './blog-listing.component.html',
  styleUrls: ['./blog-listing.component.css']
})
export class BlogListingComponent implements OnInit {
blogslist:any;
  constructor(private blogs:UserService,private router :Router) { }

  ngOnInit() {

    this.getDetails()  
  }
  
  getDetails(){
    this.blogs.listBlogs().subscribe(res => {
      this.blogslist = res;
      console.log("blogs",this.blogslist);
    });
  }

  delete(id){
    this.blogs.delete(id).subscribe(res => {
      console.log('Deleted');    
    // this.router.navigate(['blogs-listing/blogs-listing'])
    location.reload()
    });
    // this.router.navigate(['blogs-listing/blogs-listing'])
  }
  }

