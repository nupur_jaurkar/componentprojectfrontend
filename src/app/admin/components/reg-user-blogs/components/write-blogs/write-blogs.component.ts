import { Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { UserService } from '../../../../../services/user.service';
import { ReactiveFormsModule } from "@angular/forms";
import { Router } from '@angular/router';
import { FileUploader } from 'ng2-file-upload';


@Component({
  selector: 'app-write-blogs',
  templateUrl: './write-blogs.component.html',
  styleUrls: ['./write-blogs.component.css']
})
export class WriteBlogsComponent implements OnInit {

  text1: string = '<div>Hello World!</div><div>PrimeNG <b>Editor</b> Rocks</div><div><br></div>';
    
  text2: string;



  rForm: FormGroup;
  post:any;                     // A property for our submitted form
  description:string = '';
  name:string = '';   
  imagesBlog:string = '';
  titleAlert:string = 'This field is required';
  loading: boolean = false;
  file:null;

  @ViewChild('fileInput') fileInput: ElementRef;

  // fileToUpload: File = null;
  // public uploader:FileUploader = new FileUploader({});


  constructor(private fb: FormBuilder,
    private service:UserService, private router: Router,) {

      this.rForm = fb.group({
        'name' : [null, Validators.required],
        'description' : [null, Validators.compose([Validators.required, Validators.minLength(30), Validators.maxLength(9000)])],
        'validate' : '',
        // 'imagesBlog':[null,Validators.required],
        file :null
        
        // 'file':[null,Validators.required]
      });
      // console.log(this.uploader)
     }

  // addPost(post) {
  //   this.description = post.description;
  //   this.name = post.name;
  // }

  ngOnInit() {
    // this.rForm= new FormGroup({
    //   'username': new FormControl,                //username
    //   'password':new FormControl                 //password
    // })
    this.rForm.get('validate').valueChanges.subscribe(

      (validate) => {

          if (validate == '1') {
              this.rForm.get('name').setValidators([Validators.required, Validators.minLength(3)]);
              this.titleAlert = 'You need to specify at least 3 characters';
          } else {
              this.rForm.get('name').setValidators(Validators.required);
          }
          this.rForm.get('name').updateValueAndValidity();

      });
}


onFileChange(event) {
  if(event.target.files.length > 0) {
    let file = event.target.files[0];
    this.rForm.get('file').setValue(file);
  }
}

private prepareSave(): any {
  let input = new FormData();
  input.append('name', this.rForm.get('name').value);
  input.append('description', this.rForm.get('description').value);
  input.append('file', this.rForm.get('file').value);
  return input;
}

addBlogs(post:any) {
  const formModel = this.prepareSave();
    this.service.addBlogs(formModel).subscribe((res:any)=>{
      console.log("Your blog is saved!::",res)
      if(res.code=200){
        alert(res.message)
      }
    })   
}

// onFileChange(event){
//   let reader = new FileReader();

//   if(event.target.files && event.target.files.length){
//     const [file] = event.target.files;
//     reader.readAsDataURL(file);

//     reader.onload = () =>{
//       this.rForm.patchValue({
//         file : reader.result
//       });
//       // //need to run CD since file loads outside of zone
//       // this.cd.markForCheck();
//     }
//   }
// }

// handleFileInput(files: FileList) {
//   this.fileToUpload = files.item(0);
// }

}
