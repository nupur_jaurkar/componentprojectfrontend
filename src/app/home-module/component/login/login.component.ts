import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { LoginService } from '../../../services/login.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  rForm: FormGroup;
  post:any;                     // A property for our submitted form
  username:string='';       //username
  password:string = '';     //password
  //name:string = '';
  //titleAlert:string = 'This field is required';
  // ServicesService:any;
  emailPattern : "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
  showLoginWarningMessage: boolean = false;
  

  constructor(private fb: FormBuilder,
    private service:LoginService, private router: Router,private toastr: ToastrService) { 
      this.rForm = fb.group({
        'username' : [null, Validators.required],
        'password' : [null, Validators.required],
        'validate' : '',
        'email': ['', [Validators.required, Validators.pattern(this.emailPattern)]]
      });
    }

  ngOnInit() {
    this.rForm= new FormGroup({
      'username': new FormControl,                //username
      'password':new FormControl                 //password
    })
    
  }
  login(post:any) {
    this.service.login(post).subscribe((res:any)=>{
      console.log("loginservice::",res)
      // var token = res.token;
      if(res.code==200){
        
        localStorage.setItem('LoggedInUser', 'true');    
        localStorage.setItem('id',res.user._id)
        localStorage.setItem('token', res.user.token);    
        localStorage.setItem("role",res.user.role);
        // if(localStorage.getItem()=="admin")
        if(res.user.role=="admin")
        {
          this.router.navigate(['/dashboard'])
          this.toastr.success('admin login successfully');

          alert(res.message)
        }
        else
        {
          this.router.navigate(['/userdashboard']);
          alert(res.message)
        }

      }
      else{
        alert(res.message)
      }
    
      // console.log('token is: ',token)
    })
  }

  get officialEmail() {
    return this.rForm.get('email');
  }

}
