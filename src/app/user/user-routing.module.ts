import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { RegUserBlogsComponent } from './components/reg-user-blogs/reg-user-blogs.component';
import { WriteBlogsComponent } from './components/reg-user-blogs/components/write-blogs/write-blogs.component';
import { BlogListingComponent } from './components/reg-user-blogs/components/blog-listing/blog-listing.component';
import { EditBlogsComponent } from './components/reg-user-blogs/components/edit-blogs/edit-blogs.component';
// import { ManageUsersComponent } from '../admin/components/manage-users/manage-users.component';

const routes: Routes = [
 {
   path:'',
   component:DashboardComponent
 },
 {
   path:'contact-us',
   component:ContactUsComponent
 },
 {
   path:'reg-user-blogs',
   component:RegUserBlogsComponent
 },
 {
   path:'write-blogs',
   component:WriteBlogsComponent
 },
 {
  path:'blog-listing-user',
  component:BlogListingComponent
 },
 {
   path:'edit-blogs/:id',
   component:EditBlogsComponent
 },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class UserRoutingModule { }
