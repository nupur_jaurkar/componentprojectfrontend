import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { UserService } from '../../../../../services/user.service';
// import { Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';



@Component({
  selector: 'app-blog-listing',
  templateUrl: './blog-listing.component.html',
  styleUrls: ['./blog-listing.component.css']
})
export class BlogListingComponent implements OnInit {
blogslist:any;
commentslist= new Array;
rForm: FormGroup;
post:any;
// val2: number = 3;
val1: number;
// val: number = 3;
msg: string;

  constructor(private fb: FormBuilder,private blogs:UserService,private router :Router,private route:ActivatedRoute,) { 
    this.rForm = fb.group({
      'blogId' : [null, Validators],     
      'comments' : [null, Validators],
      // 'userId' : [null, Validators],
      // 'rating' : [null, Validators],
      // 'date' : [null, Validators],
      // 'rating' :[null, Validators]
    });
  }

  ngOnInit() {

    this.getDetails()  ;
    // this.commentsListing();

  }

  getDetails(){
    this.blogs.listBlogs().subscribe(res => {
      this.blogslist = res;
      console.log("blogs",this.blogslist);
    });
  }

  delete(id){
    this.blogs.delete(id).subscribe(res => {
      console.log('Deleted');    
    // this.router.navigate(['blogs-listing/blogs-listing'])
    location.reload()
    });
    // this.router.navigate(['blogs-listing/blogs-listing'])
  }

  comments(){
    // console.log("%%%%%%%%", data);
    // return;
    // this.route.params.subscribe(params =>{
   var id= localStorage.getItem('blogId')
    var userId = localStorage.getItem("id");
    this.rForm.value.userId = userId;
    this.rForm.value.blogId = id;
    this.rForm.value.rating = this.val1
    // this.rForm.value.blogId = blogId

      console.log("this.rform ", this.rForm.value)
      this.blogs.comments(this.rForm.value).subscribe((res:any)=>{
        console.log("Comment added!::",res)
        if(res.code=200){
          alert(res)
        }
      })
      location.reload();
    // })
  }

  commentsListing(){
    // var id= localStorage.getItem('blogId');
    // console.log(id,"id.....")
    var id = localStorage.getItem('blogId');
    this.blogs.commentsListing(id).subscribe((res:any)=>{
      console.log(res,"res......")
      this.commentslist = res;
    })
  }

  
blogData;
  openModal(id){
    localStorage.setItem('blogId', id);
    document.getElementById('id01').style.display='block';
    this.blogs.getBlogById(id).subscribe(res=>{
      console.log("res",res)
      this.blogData= res;
      this.commentsListing()
    })
  }
  //=========Ratings
//   handleRate(event) {
//     this.msg = "You have rated " + event.value;
// }

// handleCancel(event) {
//     this.msg = "Rating Cancelled";
// }

  //rating
  // handleRate(event) {
  //     this.msg = "You have rated " + event.value;
  // }

  // handleCancel(event) {
  //     this.msg = "Rating Cancelled";
  // }

}

